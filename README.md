Simple GitLab CI helper to cleanup long running containers left after test stages.

Usage:
```
  -dry-run
        Do not delete pods, just print what will be deleted
  -name string
        name filter, just like docker ps --filter=name=... 
  -since duration
        duration for cleanup, for example 5m
```

See [Runner hooks to the rescue](https://dev.to/fkurz/gitlab-cicd-runner-clean-up-with-pre-build-scripts-4b0g)
```
# /etc/gitlab-runner/config.toml
# ...
[[runners]]
# ...
pre_build_script = "docker run -v /var/run/docker.sock:/var/run/docker.sock  --rm softkot/docker-cleanup --name=whatever --since 5m"
```