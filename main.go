package main

import (
	"context"
	"flag"
	"github.com/docker/docker/api/types/filters"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/client"
)

var since = flag.Duration("since", 0, "duration for cleanup, for example 5m")
var name = flag.String("name", "", "name filter, just like docker ps --filter=name=... ")
var dry = flag.Bool("dry-run", false, "Do not delete pods, just print what will be deleted")

func main() {
	flag.Parse()
	filterArgs := filters.NewArgs()
	if *name != "" {
		filterArgs.Add("name", *name)
	}
	if *since == 0 {
		println("Specify duration with --since flag")
		return
	}
	cli, err := client.NewClientWithOpts(client.FromEnv)
	if err != nil {
		println(err)
		return
	}
	containers, err := cli.ContainerList(context.Background(), types.ContainerListOptions{
		All:     true,
		Filters: filterArgs,
	})
	if err != nil {
		panic(err)
	}
	now := time.Now()
	for _, container := range containers {
		delta := now.Unix() - container.Created
		if time.Second*time.Duration(delta) > *since {
			println("Deleting pod ", container.ID, strings.Join(container.Names, " "))
			if !*dry {
				if err := cli.ContainerRemove(context.Background(), container.ID, types.ContainerRemoveOptions{RemoveVolumes: true, Force: true}); err != nil {
					println("unable to remove", container.ID, err.Error())
				}
			}
		}
	}
}
