# docker buildx build --tag softkot/docker-cleanup:latest --push .
FROM golang:1.21-alpine as builder
RUN mkdir -p /build
COPY ./ /build/
WORKDIR /build
ENV CGO_ENABLED=0
RUN go build -o docker-cleanup -ldflags "-s -w"
FROM scratch
MAINTAINER Alexei Volkov <softkot@gmail.com>

EXPOSE 8001
COPY --from=builder /build/docker-cleanup /docker-cleanup
COPY --from=builder /usr/local/go/lib/time/zoneinfo.zip /
ENV ZONEINFO=/zoneinfo.zip
ENTRYPOINT ["/docker-cleanup"]
CMD []
